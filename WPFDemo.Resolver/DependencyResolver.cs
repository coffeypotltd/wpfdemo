﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using WPFDemo.ServerFunctions.Mail;
using WPFDemo.ServerFunctions.Screenshot;

namespace WPFDemo.Resolver
{
    public class DependencyResolver
    {
        public static IUnityContainer Container { get; set; }

        static DependencyResolver()
        {
            Container = new UnityContainer();
            RegisterTypes(Container);
        }

        private static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IEmailer, Emailer>();
            container.RegisterType<IScreenCapture, ScreenCapture>();
        }
    }
}
