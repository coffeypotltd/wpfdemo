﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFDemo.Messaging
{
    public class ErrorMessages
    {
        public static readonly string StartupError = @"WPF program failed on startup, see error: {0}";
        public static readonly string SendEmailError = @"Error sending email, see error:{0}. stacktrace:{1}.";
    }
}
