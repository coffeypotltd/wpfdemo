﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFDemo.Messaging
{
    public class GeneralMessages
    {
        public static readonly string EmailerAddress = @"ProgramNameImporter@companyemailaddress.com";
        public static readonly string EmailerDisplayName = @"Name Goes Here";
        public static readonly string StartupNotification = @"Ready";
        public static readonly string NLogInfoWarning = @"Call for Logger to log info was executed, but message was empty";
        public static readonly string NLogErrorWarning = @"Call for Logger to log error was executed, but error message was empty";
        public static readonly string NLogCriticalWarning = @"Call for Logger to log CRITCAL was executed, but critical message was empty";
    }
}
