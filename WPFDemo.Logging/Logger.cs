﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFDemo.Messaging;

namespace WPFDemo.Logging
{
    public class Logger : ILogger
    {
        private static NLog.Logger _logger;

        public Logger()
        {
            _logger = NLog.LogManager.GetLogger("logfile");
        }

        public void LogInfo(string message)
        {
            if (string.IsNullOrEmpty(message))
                _logger.Warn(GeneralMessages.NLogInfoWarning);
            _logger.Info(message);
        }
        public void LogError(string message)
        {
            if (string.IsNullOrEmpty(message))
                _logger.Warn(GeneralMessages.NLogErrorWarning);
            _logger.Error(message);
        }
        public void LogCritical(string message)
        {
            if (string.IsNullOrEmpty(message))
                _logger.Warn(GeneralMessages.NLogCriticalWarning);
            _logger.Fatal(message);
        }
    }
}