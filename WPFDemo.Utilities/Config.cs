﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using WPFDemo.Models.Configuration;

namespace WPFDemo.Utilities
{
    public static class Config
    {
        public static DemoConfig GetEmailInfo(string path)
        {
            DemoConfig results = new DemoConfig();
            NameValueCollection collection = ConfigurationManager.GetSection(path) as NameValueCollection;

            foreach(string result in collection)
            {
                switch (result)
                {
                    case "MailFrom":
                        results.MailFrom = collection["MailFrom"];
                        break;
                    case "Dev":
                        results.Developer = collection["Dev"];
                        break;
                    case "SA":
                        results.SA = collection["SA"];
                        break;
                    case "MailServer":
                        results.MailServer = collection["MailServer"];
                        break;
                    default:
                        break;
                }
            }

            return results;
        }

        /// <summary>
        /// Gets connection related information based on configuration.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static DemoConfig GetConnectionInfo(string path)
        {
            DemoConfig results = new DemoConfig();
            NameValueCollection collection = ConfigurationManager.GetSection(path) as NameValueCollection;

            foreach (string item in collection)
            {
                if (item == "mssqlConnection")
                {
                    results.MssqlConnection = collection["mssqlConnection"];
                }
                if( item == "mysqlConnection")
                {
                    results.MysqlConnection = collection["mysqlConnection"];
                }
            }

            return results;
        }

        /// <summary>
        /// Gets Authentication related information based on configuration.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static DemoConfig GetAuthenticationInfo(string path)
        {
            DemoConfig results = new DemoConfig();
            NameValueCollection collection = ConfigurationManager.GetSection(path) as NameValueCollection;

            foreach (string item in collection)
            {
                switch (item)
                {
                    case "365User":
                        results.Office365User = collection["365User"];
                        break;
                    case "365Password":
                        SecureString securedString = collection["365Password"].ToSecureString();
                        results.Office365Password = securedString;
                        break;
                    case "Password":
                        results.Password = collection["Password"];
                        break;
                    case "Domain":
                        results.Domain = collection["Domain"];
                        break;
                    case "User":
                        results.User = collection["User"];
                        break;
                    default:
                        break;
                }
            }

            return results;
        }
    }
}