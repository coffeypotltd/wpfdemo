﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFDemo.Notifications
{
    public interface INotifer
    {
        string DisplayInformation { get; set; }
        string TextInformation { get; set; }

        event PropertyChangedEventHandler PropertyChanged;
    }
}
