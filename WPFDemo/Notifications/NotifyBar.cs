﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFDemo.Notifications
{
    internal class NotifyBar : INotifyPropertyChanged, INotifyBar
    {
        private string _statusText;
        public string StatusText { get { return _statusText; } set { _statusText = value; this.NotifyPropertyChanged("StatusText"); } }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
