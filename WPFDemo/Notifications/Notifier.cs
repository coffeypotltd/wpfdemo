﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFDemo.Notifications
{
    internal class Notifier : INotifyPropertyChanged, INotifer
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _textInformation;
        private string _displayInformation;

        public Notifier()
        {
            this.DisplayInformation = string.Empty;
            this.TextInformation = string.Empty;
        }
        public string DisplayInformation
        {
            get { return _displayInformation; }
            set
            {
                this._displayInformation = value;
                this.NotifyPropertyChanged("DisplayInformation");
            }
        }

        public string TextInformation
        {
            get { return _textInformation; }
            set
            {
                _textInformation = value;
                NotifyPropertyChanged("TextInformation");
            }
        }

        private void NotifyPropertyChanged(String info)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
