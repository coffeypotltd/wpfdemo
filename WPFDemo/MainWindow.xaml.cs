﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFDemo.Messaging;
using WPFDemo.Notifications;
using WPFDemo.Pages;
using WPFDemo.Pages.Edit;

namespace WPFDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private AdminFunctions _admin;
        private CreatePage _create;
        private DeletePage _delete;
        private InsertPage _insert;
        private UpdatePage _update;

        public static INotifyBar _notifyBar = new NotifyBar();

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                InitializePages();

                NotificationBar_Control.DataContext = _notifyBar;
                _notifyBar.StatusText = GeneralMessages.StartupNotification;
            }
            catch(Exception ex)
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry(string.Format(ErrorMessages.StartupError, ex.Message, EventLogEntryType.Information, 101, 1));
                    throw;
                }
            }
        }

        private void Edit_Menu_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();
            _NavigationFrame.Navigate(_admin);
        }

        private void Create_Button_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();
            _NavigationFrame.Navigate(_create);
        }

        private void Update_Button_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();
            _NavigationFrame.Navigate(_update);
        }

        private void Insert_Button_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();
            _NavigationFrame.Navigate(_insert);
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();
            _NavigationFrame.Navigate(_delete);
        }

        private void InitializePages()
        {
            _admin = new AdminFunctions(mainWindow);
            _create = new CreatePage(mainWindow);
            _delete = new DeletePage(mainWindow);
            _insert = new InsertPage(mainWindow);
            _update = new UpdatePage(mainWindow);
        }
        private void DisableButtons()
        {
            Create_Button.Visibility = Visibility.Hidden;
            Update_Button.Visibility = Visibility.Hidden;
            Insert_Button.Visibility = Visibility.Hidden;
            Delete_Button.Visibility = Visibility.Hidden;
            Logo_jpg.Visibility = Visibility.Hidden;
        }
    }
}
