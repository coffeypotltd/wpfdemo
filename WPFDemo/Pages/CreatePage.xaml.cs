﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFDemo.Notifications;
using WPFDemo.ServerFunctions.Mail;
using WPFDemo.Validation;
using Unity;
using WPFDemo.Resolver;

namespace WPFDemo.Pages
{
    /// <summary>
    /// Interaction logic for CreatePage.xaml
    /// </summary>
    public partial class CreatePage : Page
    {
        private MainWindow _mainWindow;
        private INotifyBar _notification;
        private INotifer _notify;
        private PageMessages _messages;
        private Verification _verification;

        private IEmailer _emailer;
        
        public CreatePage(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            InitializeComponent();

            _notification = new NotifyBar();
            _notify = new Notifier();
            _messages = new PageMessages();
            _verification = new Verification();

            //Showing how to resolve dependcy using Microsoft Unity. 
            //Does not belong in constructor.
            _emailer = DependencyResolver.Container.Resolve<IEmailer>();
        }
    }
}
