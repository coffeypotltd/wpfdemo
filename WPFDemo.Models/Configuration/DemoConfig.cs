﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace WPFDemo.Models.Configuration
{
    public class DemoConfig
    {
        public string MssqlConnection { get; set; }
        public string MysqlConnection { get; set; }
        public string Office365User { get; set; }
        public SecureString Office365Password { get; set; }
        public string Domain { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string MailFrom { get; set; }
        public string Developer { get; set; }
        public string SA { get; set; }
        public string EndUser { get; set; }
        public string MailServer { get; set; }
        public string MailGroup { get; set; }
    }
}
