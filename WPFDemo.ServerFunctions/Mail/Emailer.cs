﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WPFDemo.Logging;
using WPFDemo.Messaging;
using WPFDemo.Models.Configuration;
using WPFDemo.Utilities;

namespace WPFDemo.ServerFunctions.Mail
{
    public class Emailer : IEmailer
    {
        private ILogger _logger;
        private DemoConfig _section;
        private DemoConfig _office365;

        public Emailer()
        {
            _logger = new Logger();
            _section = Config.GetEmailInfo("Emailees");
            _office365 = Config.GetAuthenticationInfo("Office365");
        }

        public MemoryStream memStream { get; set; }

        public void Dispose()
        {
            if (memStream != null)
            {
                memStream.Dispose();
                memStream = null;
            }
        }

        public void SendEmail(string subject, string body)
        {
            using (MailMessage email = new MailMessage())
            {
                email.From = new MailAddress(GeneralMessages.EmailerAddress, GeneralMessages.EmailerDisplayName);

                if (memStream != null)
                {
                    LinkedResource footerImg = new LinkedResource(memStream, "image/jpeg") { ContentId = "screenshot" };
                    AlternateView foot = AlternateView.CreateAlternateViewFromString(body + "<p> <img src=cid:screenshot /> </p>", null, "text/html");
                    foot.LinkedResources.Add(footerImg);

                    email.AlternateViews.Add(foot);
                }

                try
                {
                    using (SmtpClient client = new SmtpClient(_section.MailServer))
                    {
                        client.EnableSsl = true;
                        client.Credentials = new System.Net.NetworkCredential(_office365.Office365User, _office365.Office365Password);
                        client.Port = 587;
                        client.Send(email);
                    }
                }
                catch(Exception ex)
                {
                    _logger.LogError(string.Format(ErrorMessages.SendEmailError, ex.Message, ex.StackTrace));
                    throw;
                }
            }
        }
    }
}
